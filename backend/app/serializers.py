from app.models import ExUser, Projects, Members, Roles, Tasks, Columns, Boards
from rest_framework import serializers


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = ExUser
        fields = ('email', 'first_name', 'last_name')


class MemberSerializer(serializers.ModelSerializer):
    user = UserSerializer()

    class Meta:
        model = Members
        fields = ('user', 'role')


class ProjectSerializer(serializers.ModelSerializer):
    members = MemberSerializer(many=True, source='project_memberships')
    manager = UserSerializer()

    class Meta:
        model = Projects
        fields = ('name', 'description', 'members', 'manager')


class TaskSerializer(serializers.ModelSerializer):
    executor = serializers.CharField(source='executer.email')
    column = serializers.CharField(source='column.name')

    class Meta:
        model = Tasks
        fields = ('name', 'description', 'executor', 'status', 'column')


class ColumnSerializer(serializers.ModelSerializer):
    board = serializers.CharField(source='board.name')

    class Meta:
        model = Columns
        fields = ('name', 'board')


class BoardsSerializer(serializers.ModelSerializer):
    project_name = serializers.CharField(source='project.name')

    class Meta:
        model = Boards
        fields = ('project_name', 'name', 'description')
