from rest_framework.test import APITestCase, APIRequestFactory
from app.models import ExUser
import json

from app.views import user_data


class FuncTestCase(APITestCase):

    def setUp(self):
        ExUser.objects.create_user(email="zayycev22@gmail.com", password="agent368", first_name="Yuri",
                                   last_name="aboba")

    def test_my_api_endpoint(self):
        url = '/api/check_auth'
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, {'authenticated': False})

    def test_valid_reg(self):
        url = '/api/reg'
        data = {"email": "zayycev222@gmail.com", "password": "agent368", "first_name": "Yuri", "last_name": "aboba"}
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.data, {"Status": "logged in"})
        self.assertEqual(ExUser.objects.count(), 2)

    def test_valid_login(self):
        url = '/api/login'
        data = {"email": "zayycev22@gmail.com", "password": "agent368"}
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, {"Status": "logged in"})

    def test_invalid_login(self):
        url = '/api/login'
        data = {"email": "zayycev2222@gmail.com", "password": "agent368"}
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data, {"Status": "dismiss"})

    def test_user_data(self):
        factory = APIRequestFactory()
        user = ExUser.objects.get(email='zayycev22@gmail.com')
        view = user_data

        request = factory.get('/api/user_data')
        request.user = user
        response = view(request)
        self.assertEqual(response.data, {'email': 'zayycev22@gmail.com', 'first_name': 'Yuri', 'last_name': 'aboba'})

