from django.apps import apps
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import PermissionsMixin, AbstractUser, UserManager
from django.db import models
from django.utils import timezone


class EmailUserManager(UserManager):
    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """
        Create and save a user with the given username, email, and password.
        """
        if not email:
            raise ValueError("The given email must be set")
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email=None, password=None, **extra_fields):
        """    
    The create_user function is a helper function that creates and saves a User with the given email and password.
    The default value for is_staff is False, which means the user will not be able to log in to the Django admin site.
    The default value for is_superuser is False, which means that the user won’t have all permissions without explicitly being assigned.
    
    :param self: Refer to the class itself
    :param email: Create a user with the given email
    :param password: Set the password for the user
    :param **extra_fields: Pass in any additional fields that you want to add to the user model
    :return: A user object
    :doc-author: zayycev22
    """
        extra_fields.setdefault("is_staff", False)
        extra_fields.setdefault("is_superuser", False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email=None, password=None, **extra_fields):
        """    
        The create_superuser function is a helper function that creates a new user with the given email and password.
        It also sets the is_staff and is_superuser flags to True, which gives this user access to the Django admin site.
        
        :param self: Refer to the class itself
        :param email: Set the email address of the user
        :param password: Set the password for the user
        :param **extra_fields: Pass any additional fields you want to set when creating the user
        :return: An instance of the user model
        :doc-author: zayycev22
        """
        extra_fields.setdefault("is_staff", True)
        extra_fields.setdefault("is_superuser", True)

        if extra_fields.get("is_staff") is not True:
            raise ValueError("Superuser must have is_staff=True.")
        if extra_fields.get("is_superuser") is not True:
            raise ValueError("Superuser must have is_superuser=True.")

        return self._create_user(email, password, **extra_fields)


class ExUser(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(blank=True, unique=True)
    first_name = models.CharField(max_length=192)
    last_name = models.CharField(max_length=192)
    is_staff = models.BooleanField(
        "staff status",
        default=False,
        help_text="Designates whether the user can log into this admin site.",
    )
    is_active = models.BooleanField(
        "active",
        default=True,
        help_text=(
            "Designates whether this user should be treated as active. "
            "Unselect this instead of deleting accounts."
        ),
    )
    date_joined = models.DateTimeField("date joined", default=timezone.now)
    objects = EmailUserManager()

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []

    def __str__(self):
        return self.email

    class Meta:
        verbose_name = "Пользователь"
        verbose_name_plural = "Пользователи"


class Roles(models.Model):
    name = models.CharField(max_length=30)
    description = models.TextField()

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Роль"
        verbose_name_plural = "Роли"


class Projects(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField()
    members = models.ManyToManyField(ExUser, through="Members", related_name='projects_as_member')
    manager = models.ForeignKey(ExUser, on_delete=models.SET_NULL, null=True, related_name='project_as_manager')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Проект"
        verbose_name_plural = "Проекты"


class Members(models.Model):
    user = models.ForeignKey(ExUser, on_delete=models.CASCADE, related_name='user_memberships')
    project = models.ForeignKey(Projects, on_delete=models.CASCADE, related_name='project_memberships')
    role = models.ForeignKey(Roles, on_delete=models.SET_NULL, null=True, default=None)

    class Meta:
        verbose_name = "Участник"
        verbose_name_plural = "Участники"
        unique_together = ('user', 'project')


class Boards(models.Model):
    project = models.ForeignKey(Projects, on_delete=models.CASCADE, related_name='boards')
    name = models.CharField(max_length=30)
    description = models.TextField()

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Доска"
        verbose_name_plural = "Доски"


class Columns(models.Model):
    name = models.CharField(max_length=30)
    board = models.ForeignKey(Boards, on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Колонка"
        verbose_name_plural = "Колонки"


class Tasks(models.Model):
    name = models.CharField(max_length=30)
    description = models.TextField()
    column = models.ForeignKey(Columns, on_delete=models.CASCADE)
    executer = models.ForeignKey(ExUser, on_delete=models.SET_NULL, null=True, default=None)
    status = models.BooleanField(default=False)
    due_time = models.DateField(default=None, null=True)
    date_created = models.DateField(auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Задание"
        verbose_name_plural = "Задания"
