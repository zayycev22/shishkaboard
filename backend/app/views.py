from django.db.models import Q
from django.http import Http404
from rest_framework.views import APIView

from app.forms import AuthForm, RegForm
from app.models import ExUser, Projects, Boards, Columns, Members, Tasks
from app.serializers import UserSerializer, ProjectSerializer, MemberSerializer, BoardsSerializer, TaskSerializer, \
    ColumnSerializer
from django.contrib.auth import authenticate, login, logout
from django.db import IntegrityError
from django.middleware.csrf import get_token
from rest_framework import permissions
from rest_framework.decorators import permission_classes, api_view
from pprint import pprint
from rest_framework.response import Response
from rest_framework.request import Request as RestRequest


@api_view(http_method_names=['GET'])
@permission_classes((permissions.AllowAny,))
def check_auth(request: RestRequest):
    """
    The check_auth function is a view that checks if the user is authenticated.
    If they are, it returns a JSON response with an &quot;authenticated&quot; key set to true.
    Otherwise, it returns false.
    
    :param request: WSGIRequest: Get the user object from the request
    :return: A response object
    :doc-author: zayycev22
    """
    if request.user.is_authenticated:
        return Response({"authenticated": True})
    else:
        return Response({"authenticated": False})


@api_view(http_method_names=['POST'])
@permission_classes((permissions.AllowAny,))
def try_login(request: RestRequest):
    """
    The try_login function is called when the user submits a login form.
    It takes in the request object, which contains all of the information about
    the HTTP request that was made to this endpoint. It then creates an AuthForm
    object with that data and checks if it's valid (i.e., if it passes validation). 
    If so, we authenticate using Django's built-in authentication system and log 
    the user in by calling login(request, user). If not, we return a 400 error code.
    
    :param request: WSGIRequest: Get the form data from the request
    :return: A response object, which is a django rest framework object
    :doc-author: zayycev22
    """
    form = AuthForm(request.POST)
    if form.is_valid():
        username = form.cleaned_data['email']
        password = form.cleaned_data['password']
        user = authenticate(email=username, password=password)
        if user is not None:
            login(request, user)
            return Response({"Status": "logged in"}, status=200)
        else:
            return Response({"Status": "dismiss"}, status=400)
    else:
        return Response(form.errors, status=400)


@api_view(http_method_names=['POST'])
@permission_classes((permissions.AllowAny,))
def try_reg(request: RestRequest):
    """
    The try_reg function is called when a user attempts to register.
    It takes the request object and creates a RegForm object from it.
    If the form is valid, then it creates an ExUser with the given information, logs them in, and returns a 200 status code.
    Otherwise, it returns 400.

    :param request: WSGIRequest: Pass the request object to the function
    :return: A response object
    :doc-author: zayycev22
    """
    form = RegForm(request.POST)
    if form.is_valid():
        username = form.cleaned_data['email']
        password = form.cleaned_data['password']
        first_name = form.cleaned_data['first_name']
        last_name = form.cleaned_data['last_name']
        try:
            user = ExUser.objects.create_user(email=username, password=password, last_name=last_name,
                                              first_name=first_name)
        except IntegrityError:
            return Response({"detail": "user exists"}, status=400)
        else:
            login(request, user)
        return Response({"Status": "logged in"}, status=201)
    else:
        return Response(form.errors, status=400)


@api_view(http_method_names=['GET'])
@permission_classes((permissions.AllowAny,))
def try_logout(request: RestRequest):
    """
    The try_logout function is a view that logs out the user.
    It takes in a request and returns a response with status 200 if successful.
    
    :param request: WSGIRequest: Get the request object
    :return: A response object with a status code of 200
    :doc-author: zayycev22
    """
    logout(request)
    return Response({"Status": "logged out"}, status=200)


@api_view(http_method_names=['GET'])
def csrf(request: RestRequest):
    """
    The csrf function is a view that returns the CSRF token required for
        POST forms.  You can use it in your templates like this:
    
    :param request: WSGIRequest: Get the token from the request
    :return: The csrf token as a json object
    :doc-author: zayycev22
    """
    token = get_token(request)
    return Response({'csrf': token})


@api_view(http_method_names=['GET'])
def user_data(request: RestRequest):
    """
    The user_data function is a view that returns the user's data in JSON format.
    It takes one parameter, request, which is an HTTP request object. It uses the
    UserSerializer class to serialize the user's data into JSON format and then it
    returns a Response object containing that serialized data.
    
    :param request: WSGIRequest: Get the user data
    :return: A response object, which is a special type of object that django rest framework uses to send data back to the client
    :doc-author: zayycev22
    """
    data = UserSerializer(request.user)
    return Response(data.data)


@api_view(http_method_names=['GET'])
def user_projects(request: RestRequest):
    """
    The user_projects function returns a list of projects that the user is either a manager or member of.
        The function takes in one parameter, request, which is an HTTP request object.
        The function first gets the user from the request object and then queries for all projects where
        either the manager or members field contains this user. It then serializes these queryset objects into JSON format
        using ProjectSerializer and returns them as a response.

    :param request: WSGIRequest: Get the user from the request
    :return: A list of projects that the user is either a manager or member of
    :doc-author: zayycev22
    """
    user = request.user
    projects = Projects.objects.select_related('manager').prefetch_related(
        'members',
        'members__user_memberships__user',
        'members__user_memberships__role',
    ).filter(Q(manager=user) | Q(members=user)).only(
        "members__email",
        "members__first_name",
        "members__last_name",
        "manager__email",
        "manager__last_name",
        "manager__first_name",
        "name",
        "description"
    )
    data = ProjectSerializer(projects, many=True)
    return Response(data.data)


@api_view(http_method_names=['GET'])
def user_project(request: RestRequest, name: str):
    """
    The user_project function is a view that returns the project with the given pk if it exists and
    the user is either a manager or member of that project. If not, then an error message will be returned.

    :param request: WSGIRequest: Get the user from the request
    :param pk: int: Get the project id from the url
    :return: A response object with a status code of 200 if the user is authorized to access
    :doc-author: zayycev22
    """
    user = request.user
    project = Projects.objects.select_related('manager').prefetch_related(
        'members',
        'members__user_memberships__user',
        'members__user_memberships__role',
    ).filter((Q(manager=user) | Q(members=user)) & Q(name=name)).only(
        "members__email",
        "members__first_name",
        "members__last_name",
        "manager__email",
        "manager__last_name",
        "manager__first_name",
        "name",
        "description"
    )
    if project.exists():
        project = project.first()
        data = ProjectSerializer(project)
        return Response(data.data, status=200)
    else:
        return Response({'detail': 'Not allowed'}, status=400)


@api_view(http_method_names=['PUT'])
def change_password(request: RestRequest):
    """
    The change_password function is used to change the password of a user.
    It takes in an old_password and new_password, checks if the old_password matches
    the current password for that user, and then changes it to the new one. It returns
    a status code 200 if successful or 400 otherwise.

    :param request: RestRequest: Get the user and data from the request
    :return: A response object
    :doc-author: zayycev22
    """
    old_password = request.data['old_password']
    new_password = request.data['new_password']
    user = request.user
    if user.check_password(old_password):
        user.set_password(new_password)
        return Response({'status': 'password changed'}, status=200)
    else:
        return Response({'status': 'wrong password'}, status=400)


@api_view(http_method_names=['POST'])
def create_project(request: RestRequest):
    """
    The create_project function creates a new project.

    :param request: RestRequest: Get the user from the request
    :return: A response object
    :doc-author: zayycev22
    """
    name = request.data['name']
    description = request.data['description']
    Projects.objects.create(manager=request.user, name=name, description=description)
    return Response({'status': f'project {name} created'}, status=200)


@api_view(http_method_names=['POST'])
def create_board(request: RestRequest, project_name: str):
    """
    The create_board function creates a new board for the project specified by the user.
        The function takes in a request object, which contains data about the project and board to be created.
        It then uses this information to create a new Board object using Django's ORM, and saves it into the database.

    :param request: RestRequest: Get the data from the request
    :return: A response object with the status of 200
    :doc-author: zayycev22
    """
    board_name = request.data['board_name']
    board_description = request.data['board_description']
    user = ExUser.objects.get(id=request.user.id)
    project = Projects.objects.get(manager=user, name=project_name)
    Boards.objects.create(name=board_name, description=board_description, project=project)
    return Response({'status': f'board {board_name} created'}, status=200)


@api_view(http_method_names=['GET'])
def board_list(request: RestRequest, project_name: str):
    """
    The board_list function is used to retrieve all boards associated with a project.
        The function takes in the request and project_name as parameters. It then retrieves the user from the database,
        using their id which is stored in request.user.id, and stores it in a variable called user.

    :param request: RestRequest: Get the user id from the request
    :param project_name: str: Filter the boards by project name
    :return: A list of boards
    :doc-author: zayycev22
    """
    user = ExUser.objects.get(id=request.user.id)
    projects = Projects.objects.filter((Q(manager=user) | Q(members=user)) & Q(name=project_name))
    if projects.exists():
        boards = Boards.objects.select_related('project').filter(project=projects.first())
        if boards.exists():
            data = BoardsSerializer(boards, many=True)
            return Response(data.data)


@api_view(http_method_names=['GET'])
def column_list(request: RestRequest, project_name: str, board_name):
    """
    The column_list function is used to retrieve all columns associated with a board.
        The function takes in the request, project_name and board_name as parameters.
        It then uses the ExUser model to get the user id from the request object.
        Next it uses Columns model to select related boards and filter them by:

    :param request: RestRequest: Get the user id
    :param project_name: str: Get the project name from the url
    :param board_name: Find the board that we want to add a column to
    :return: A list of columns in a board
    :doc-author: zayycev22
    """
    user = ExUser.objects.get(id=request.user.id)
    columns = Columns.objects.select_related('board').filter(
        (Q(board__project__manager=user) | Q(board__project__members=user)) & Q(
            board__name=board_name) & Q(board__project__name=project_name))
    if columns.exists():
        data = ColumnSerializer(columns, many=True)
        return Response(data.data)


@api_view(http_method_names=['GET'])
def task_list(request: RestRequest, project_name: str, board_name, column_name):
    """
    The task_list function is used to retrieve all tasks in a column.
        Args:
            request (RestRequest): The request object that contains information about the current HTTP request.
            project_name (str): The name of the project containing the board and column with tasks to be retrieved.
            board_name (str): The name of the board containing the column with tasks to be retrieved.
            column_name (str): The name of the column containing tasks to be retrieved.

    :param request: RestRequest: Get the user id from the request
    :param project_name: str: Filter the projects by name
    :param board_name: Get the board name from the url
    :param column_name: Filter the tasks that are returned
    :return: A list of tasks that are in the column with the specified name, which is located on
    :doc-author: zayycev22
    """
    user = ExUser.objects.get(id=request.user.id)
    tasks = Tasks.objects.select_related('executer', 'column').prefetch_related('column__board',
                                                                                'column__board__project').filter(
        (Q(column__board__project__manager=user) | Q(column__board__project__members=user)) &
        Q(column__name=column_name) & Q(column__board__name=board_name) & Q(
            column__board__project__name=project_name))
    pprint(str(tasks.query))
    if tasks.exists():
        data = TaskSerializer(tasks, many=True)
        return Response(data.data)


@api_view(http_method_names=['POST'])
def create_column(request: RestRequest):
    """
    The create_column function creates a column in the database.
        Args:
            request (RestRequest): The request object that is passed to the function.

    :param request: RestRequest: Get the data from the request
    :return: A response object with a status code of 200
    :doc-author: zayycev22
    """
    project_name = request.data['project_name']
    board_name = request.data['board_name']
    column_name = request.data['column_name']
    project = Projects.objects.get(name=project_name, manager=request.user)
    board = Boards.objects.get(name=board_name, project=project)
    Columns.objects.create(name=column_name, board=board)
    return Response({'status': f'column {column_name} created'}, status=200)


@api_view(http_method_names=['POST'])
def create_task(request: RestRequest):
    """
    The create_task function creates a new task in the database.
        It takes the following parameters:
            request - The RestRequest object containing all of the information about this request.

    :param request: RestRequest: Get the data from the request
    :return: The status of the task creation
    :doc-author: zayycev22
    """
    project_name = request.data['project_name']
    board_name = request.data['board_name']
    column_name = request.data['column_name']
    task_name = request.data['task_name']
    task_description = request.data['task_description']
    executor_email = request.data['executor']
    executor = ExUser.objects.get(email=executor_email)
    project = Projects.objects.get(manager=request.user, name=project_name)
    board = Boards.objects.get(project=project, name=board_name)
    column = Columns.objects.get(column_name=column_name, board=board)
    Tasks.objects.create(column=column, name=task_name, description=task_description, executor=executor)
    return Response({"status", f"task {task_name} created"})


@api_view(http_method_names=['POST'])
def add_member(request: RestRequest, name: str):
    """
    The add_member function adds a member to the project.
        Args:
            request (RestRequest): The request object that contains the data for adding a new member.

    :param request: RestRequest: Get the data from the request
    :return: A response object with a status of 200
    :doc-author: zayycev22
    """
    member_email = request.data['member_email']
    new_member = ExUser.objects.get(email=member_email)
    project = Projects.objects.get(name=name, manager=request.user)
    project.members.add(new_member, through_defaults={'role': None})
    return Response({'status': f'member {member_email} added'}, status=200)


@api_view(http_method_names=['GET'])
def project_members(request: RestRequest, name: str):
    """
    The project_members function is used to retrieve all members of a project.
        ---
        parameters:
            - name: project_id
              description: The id of the project whose members are being retrieved.
                           This is an integer value.
                           It must be passed in as part of the URL path, not as a query parameter or form data field.
                           For example, if you want to get all members for Project with id=3,
                           then your request URL should look like this: http://localhost/api/projects/3/members/.

    :param request: RestRequest: Get the request object
    :param project_id: int: Filter the members of a specific project
    :return: A list of members in a project
    :doc-author: zayycev22
    """
    members = Members.objects.filter(name=name).select_related('user').only(
        "user__email",
        "user__first_name",
        "user__last_name"
    )
    if members.exists():
        return Response(MemberSerializer(members, many=True).data, status=200)
    else:
        return Response({'status': f'project with name {name} does not exists'}, status=400)


class TaskView(APIView):
    @staticmethod
    def get_object(project_name: str, board_name, column_name, task_name, user, manager=False) -> Tasks:
        """
        The get_object function is used to retrieve a task object from the database.
        It takes in four parameters: project_name, board_name, column_name and task_name.
        The function then uses these parameters to query the database for a matching Tasks object.
        If no such object exists it raises an Http404 error.

        :param project_name: str: Get the project name from the url
        :param board_name: Get the board object from the database
        :param column_name: Get the column object
        :param task_name: Get the task name from the url
        :param user: Check if the user is a manager of the project
        :param manager: Check if the user is the manager of a project
        :return: A single task object
        :doc-author: zayycev22
        """
        try:
            if manager:
                task = Tasks.objects.get(
                    Q(column__board__project__name=project_name) & Q(column__board__name=board_name) & Q(
                        column__name=column_name) & Q(name=task_name) & Q(column__board__project__manager=user))
            else:
                task = Tasks.objects.get(
                    Q(column__board__project__name=project_name) & Q(column__board__name=board_name) & Q(
                        column__name=column_name) & Q(name=task_name))
            return task
        except Tasks.DoesNotExist:
            raise Http404

    def get(self, request: RestRequest, project_name: str, board_name, column_name, task_name):
        """
        The get function is used to retrieve a single task from the database.
            It takes in a request, project_name, board_name, column_name and task name.
            The user is retrieved from the request object and then used to get the
            correct task using get_object(). The data of that object is serialized into JSON format
            using TaskSerializer() and returned as a response with status code 200.

        :param self: Represent the instance of the class
        :param request: RestRequest: Get the user id from the request
        :param project_name: str: Get the project name from the url
        :param board_name: Identify the board that the task is in
        :param column_name: Get the column object from the database
        :param task_name: Get the task object from the database
        :return: A task object, which is then serialized to json
        :doc-author: zayycev22
        """
        user = ExUser.objects.get(id=request.user.id)
        task = self.get_object(project_name, board_name, column_name, task_name, user)
        data = TaskSerializer(task)
        return Response(data.data, status=200)

    def delete(self, request: RestRequest, project_name: str, board_name, column_name, task_name, format=None):
        """
        The delete function is used to delete a task.
            Args:
                request (RestRequest): The request object that contains the user's credentials and other information.
                project_name (str): The name of the project that contains the board, column, and task to be deleted.
                board_name (str): The name of the board that contains the column and task to be deleted.  This is not used in this function but it must be included for routing purposes in Django Rest Framework.  It will always have a value of 'default'.
                column_name (str): The name of the column

        :param self: Refer to the current object
        :param request: RestRequest: Get the user information from the request
        :param project_name: str: Get the project name from the url
        :param board_name: Identify the board that contains the column
        :param column_name: Get the column that the task is in
        :param task_name: Find the task that is to be deleted
        :param format: Specify the format of the response
        :return: A response object with status 200
        :doc-author: zayycev22
        """
        user = ExUser.objects.get(id=request.user.id)
        task = self.get_object(project_name, board_name, column_name, task_name, user, manager=True)
        task.delete()
        return Response({'detail': f"successfully deleted task {task_name}"}, status=200)
