from app.views import *
from django.urls import path

urlpatterns = [
    path('check_auth', check_auth),
    path('login', try_login),
    path('reg', try_reg),
    path('token', csrf),
    path('user_data', user_data),
    path('logout', try_logout),
    path('change_password', change_password),
    path('projects/', user_projects),
    path('projects/<str:name>', user_project),
    path('projects/<str:project_name>/boards/', board_list),
    path('projects/<str:project_name>/boards/<str:board_name>/columns/', column_list),
    path('projects/<str:project_name>/boards/<str:board_name>/columns/<str:column_name>/tasks/', task_list),
    path('projects/<str:project_name>/boards/<str:board_name>/columns/<str:column_name>/tasks/<str:task_name>', TaskView.as_view()),
    path('projects/<str:name>/members/', project_members),
    path('projects/<str:name>/members/add', add_member),
    path('projects/<str:project_name>/create_board', create_board),
    path('create_project', create_project),
    path('create_column', create_column),
    path('create_task', create_task),
]
