from django.contrib import admin
from .models import Projects, Members, Roles, Boards, Columns, Tasks, ExUser


class TasksInline(admin.StackedInline):
    model = Tasks


class MembersInline(admin.TabularInline):
    model = Members
    extra = 1


class BoardsInline(admin.StackedInline):
    model = Boards
    extra = 1
    inlines = [TasksInline]


class ColumnsInline(admin.StackedInline):
    model = Columns
    extra = 1


@admin.register(Projects)
class ProjectsAdmin(admin.ModelAdmin):
    inlines = [MembersInline, BoardsInline]


@admin.register(Members)
class MembersAdmin(admin.ModelAdmin):
    model = Members
    extra = 0


@admin.register(Boards)
class BoardsAdmin(admin.ModelAdmin):
    inlines = [ColumnsInline]


@admin.register(Columns)
class ColumnsAdmin(admin.ModelAdmin):
    inlines = [TasksInline]


@admin.register(Tasks)
class TaskAdmin(admin.ModelAdmin):
    model = Tasks


@admin.register(Roles)
class RolesAdmin(admin.ModelAdmin):
    pass


admin.site.register(ExUser)
