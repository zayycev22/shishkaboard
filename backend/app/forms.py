from django import forms


class AuthForm(forms.Form):
    email = forms.EmailField()
    password = forms.CharField()


class RegForm(forms.Form):
    email = forms.EmailField()
    password = forms.CharField()
    first_name = forms.CharField()
    last_name = forms.CharField()

