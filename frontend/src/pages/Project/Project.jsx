import React from 'react';
import {Button, Dialog, DialogActions, DialogContent, TextField} from "@mui/material";
import open from '../../assets/vector_open.svg'
import {useEffect, useState} from "react";
import axios from "axios";
import {useNavigate} from "react-router-dom";

const Project = () => {

    const [isAuth, setIsAuth] = useState(false)
    const [projects, setProjects] = useState([])
    const navigateTo = useNavigate()

    useEffect(() => {
        axios.get("/check_auth").then(res => {
            setIsAuth(res.data.authenticated)
            if(res.data.authenticated === false) navigateTo("/auth")

        }).catch(err => {
            navigateTo("/auth")
        })
    }, [])

    return (
        <div className={"baseFlex"}>
            <div className={"baseContainer"}>
                <div className={"titleBaseFlex"}>
                    <div className={"titleWhite"}>Проект 1</div>
                </div>
                    <div className={"projectDescription"}>Spent degraf. Beniss anarad hexadögt, paramide naling.
                        Rertad renar, opassade polytt sonyn. Nirade nysat liksom krov antropobel av epide i tenivis lynar bogt.
                        Jisat osade och telingar epidynde. Du kan vara drabbad.
                    </div>
                <div style={{display: "flex", flexDirection:"row", margin: 40, width: "60vw", justifyContent: "space-between"}}>
                    <div className={"status"}>
                        <div className={"textStatus"}>Владелец проекта</div>
                    </div>
                    <Button style={{color: "#525252"}}>
                        Изменить карточку проекта
                    </Button>
                </div>
                <div style={{display:"flex", flexDirection: "row", justifyContent: "flex-start", alignItems: "center", marginBottom: 20}}>
                    <div className={"tableProject"}>Таблица 1</div>
                    <img src={open} style={{cursor: "pointer", marginLeft: "20px"}}/>
                </div>
                <div style={{display:"flex", flexDirection: "row", justifyContent: "flex-start", alignItems: "center", marginBottom: 20}}>
                    <div className={"tableProject"}>Таблица 2</div>
                    <img src={open} style={{cursor: "pointer", marginLeft: "20px"}}/>
                </div>
                <div style={{display:"flex", flexDirection: "row", justifyContent: "flex-start", alignItems: "center", marginBottom: 20}}>
                    <div className={"tableProject"}>Таблица 3</div>
                    <img src={open} style={{cursor: "pointer", marginLeft: "20px"}}/>
                </div>
            </div>
        </div>
    );
};

export default Project;
