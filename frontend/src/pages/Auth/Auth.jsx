import React, {useEffect, useState} from 'react';
import {Tab, Tabs} from "@mui/material";
import {Registration} from "../../components/Registration/Registration.jsx";
import {Login} from "../../components/Login/Login.jsx";
import shishkaImg from '../../assets/ShishkaBoard.svg'
import Rectangle from '../../assets/Rectangle.svg'
import checkListImg from '../../assets/checkListImg.svg'
import axios from "axios";

const Auth = () => {

    const [tab, setTab] = useState(0)

    useEffect(() => {
        axios.get("/logout").then(res => {

        })
    }, [])

    return (
        <div className={'loginContainer'}>
            <img src={Rectangle}/>
            <img src={shishkaImg}/>
            <img src={checkListImg}/>
            <Tabs style={{marginTop:"50px"}} value={tab} onChange={(e, newValue) => setTab(newValue)} aria-label="basic tabs example">
                <Tab label="Вход" value={0}/>
                <Tab label="Регистрация" value={1}/>
            </Tabs>
            {tab === 0 &&
                <Login/>
            }
            {tab === 1 &&
                <Registration/>
            }
        </div>
    );
};

export default Auth;
