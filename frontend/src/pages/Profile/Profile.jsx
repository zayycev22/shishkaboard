import React, {useEffect, useState} from 'react';
import {Button, Dialog, DialogActions, DialogContent, TextField} from "@mui/material";
import axios from "axios";

const Profile = () => {

    const [email, setEmail] = useState(null)
    const [firstName, setFirstName] = useState(null)
    const [lastName, setLastName] = useState(null)
    const [openDialog, setOpenDialog] = useState(false)
    const [oldPassword, setOldPassword] = useState(null)
    const [firstPassword, setFirstPassword] = useState(null)
    const [secondPassword, setSecondPassword] = useState(null)


    function fetchData(){
        axios.get("/user_data").then(res => {
            setEmail(res.data.email)
            setFirstName(res.data.first_name)
            setLastName(res.data.last_name)
        })
    }

    function saveData(){
        console.log(email)
    }

    function openPassword(){
        setOpenDialog(true)
    }

    function savePassword(){
        if(firstPassword === secondPassword) {
            axios.get("/token").then(res => {
                console.log(res.data.csrf)
                axios.defaults.headers.common['X-CSRFToken'] = res.data.csrf;
                let data = {old_password: oldPassword, new_password: firstPassword}
                axios.put("/change_password", data).then(res => {
                    setOpenDialog(false)
                })
            })
        }
    }

    useEffect(() => {
        fetchData()
    }, [])

    return (
        <div className={"baseFlex"}>
            <div className={"baseContainer"}>
                <div className={"titleBaseFlex"}>
                    <div className={"titleWhite"}>Учетная запись</div>
                </div>
                <TextField
                    disabled
                    value = {email}
                    variant="outlined"
                    helperText="Email"
                    type="email"
                    onChange={e=> setEmail(e.target.value)}
                    style={{width: "90%", margin: 10}}
                />
                <TextField
                    disabled
                    value={lastName}
                    variant="outlined"
                    onChange={e=> setLastName(e.target.value)}
                    helperText="Фамилия"
                    style={{width: "90%", margin: 10}}
                />
                <TextField
                    disabled
                    value={firstName}
                    variant="outlined"
                    onChange={e=> setFirstName(e.target.value)}
                    helperText="Имя"
                    style={{width: "90%", margin: 10}}
                />
                <Button onClick={openPassword}>Изменить пароль</Button>
                <Button style={{marginBottom:"10px", marginTop:"10px"}} onClick={saveData}>
                    Сохранить изменения
                </Button>
            </div>
            <Dialog
                aria-labelledby="DialogTitle"
                aria-describedby="DialogDescription"
                open={openDialog}
                onClose={() => setOpenDialog(false)}
                fullWidth
                maxWidth="md"
            >
                <DialogContent style={{ height: 600 }}>
                    <div className={"baseFlex"}>
                        <TextField
                            value={oldPassword}
                            variant="outlined"
                            onChange={e=> setOldPassword(e.target.value)}
                            helperText="Старый пароль"
                            type={"password"}
                            style={{width: "90%", margin: 10}}
                        />
                        <TextField
                            value={firstPassword}
                            variant="outlined"
                            onChange={e=> setFirstPassword(e.target.value)}
                            helperText="Новый пароль"
                            type={"password"}
                            style={{width: "90%", margin: 10}}
                        />
                        <TextField
                            value={secondPassword}
                            variant="outlined"
                            onChange={e=> setSecondPassword(e.target.value)}
                            helperText="Повторите новый пароль"
                            type={"password"}
                            style={{width: "90%", margin: 10}}
                        />
                        <Button onClick={savePassword}>Сохранить пароль</Button>
                    </div>
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => setOpenDialog(false)}>Закрыть</Button>
                </DialogActions>
            </Dialog>
        </div>
    );
};

export default Profile;

const test = {
    full_name: "Астанин Денис Васильевич",
    email: "densof161922@gmail.com",
}
