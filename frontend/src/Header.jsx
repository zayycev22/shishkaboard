import React, {useEffect, useState} from "react";
import {Button, Navbar} from "flowbite-react";
import Shishka from './assets/shishka.svg'
import Logo from './assets/logo.svg'
import axios from "axios";
import {useNavigate} from "react-router-dom";

export function MyHeader() {

    const [isAuth, setIsAuth] = useState(false)
    const navigateTo = useNavigate()

    const navbarStyle = {
        backgroundColor: 'black',
    };


    useEffect(() => {
        axios.get("/check_auth").then(res => {
            setIsAuth(res.data.authenticated)
        })
    }, [])

    function logout() {
        axios.get("/logout").then(res => {
                axios.get("/check_auth").then(res => {
                    setIsAuth(res.data.authenticated)
                })
            }
        )
    }

    return (
        <Navbar
            fluid={true}
            rounded={true}
            style={navbarStyle}
        >
            <Navbar.Brand href="/">
                <img
                    src={Logo}
                    className="mr-3 h-6 sm:h-7"
                    alt="Logo"
                />
                <img
                    src={Shishka}
                    className="mr-3 h-6 sm:h-6"
                    alt="Company name"
                />
            </Navbar.Brand>
            {isAuth &&
                <div className="flex md:order-2">
                    <a href={"/profile"}>
                        <Button>
                            Мой профиль
                        </Button>
                    </a>
                </div>
            }
            {isAuth &&
                <div className="flex md:order-2">
                    <a href={"/auth"} onClick={logout}>
                        <Button>
                            Выход
                        </Button>
                    </a>
                </div>
            }
            <Navbar.Collapse>
                <Navbar.Link
                    href="/navbars"
                    active={true}>
                    Home
                </Navbar.Link>
                <Navbar.Link href="/navbars" className={"text-white"}>
                    About
                </Navbar.Link>
                <Navbar.Link href="/navbars" className={"text-white"}>
                    Services
                </Navbar.Link>
                <Navbar.Link href="/navbars" className={"text-white"}>
                    Pricing
                </Navbar.Link>
                <Navbar.Link href="/navbars" className={"text-white"}>
                    Contact
                </Navbar.Link>
            </Navbar.Collapse>
        </Navbar>)
}
