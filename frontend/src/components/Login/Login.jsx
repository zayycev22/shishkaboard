import React, {useRef} from "react";
import axios from "axios";
import * as qs from 'qs'
import {Button} from "@mui/material";
import {useNavigate} from "react-router-dom";

export function Login() {

    const refEmail = useRef()
    const refPassword = useRef()
    const navigate = useNavigate();

    function handleClick(){
        axios.post("/login", qs.stringify({email: refEmail.current.value, password: refPassword.current.value,})).then(res => {
            axios.get("/check_auth").then(r => {
                window.location.href = "/"
            })
        })
    }

    return (
        <form className={"w-full sm:w-1/3 mx-auto formContainer"}>
            <div className="mb-6 loginField">
                <label htmlFor="email"
                       className="labelLoginField block mb-2 text-sm font-medium text-gray-900 dark:text-white">
                    Почта</label>
                <input ref={refEmail} type="email" id="email"
                       className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 dark:shadow-sm-light"
                       placeholder="name@flowbite.com" required/>
            </div>
            <div className="mb-6 loginField">
                <label htmlFor="password"
                       className="labelLoginField block mb-2 text-sm font-medium text-gray-900 dark:text-white">
                    Пароль</label>
                <input ref={refPassword} type="password" id="password"
                       className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 dark:shadow-sm-light"
                       />
            </div>
            <Button style={{margin:10}} variant="contained" color="primary" onClick={handleClick}>
                ВОЙТИ
            </Button>
        </form>
    );
}
