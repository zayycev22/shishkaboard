import React, {useEffect, useRef, useState} from "react";
import axios from "axios";
import * as qs from 'qs'
import {Button} from "@mui/material";
import {useNavigate} from "react-router-dom";


export function Registration() {

    const navigate = useNavigate();

    const refEmail = useRef()
    const refPassword = useRef()
    const refFirstName = useRef()
    const refLastName = useRef()

    function handleClick(){
        axios.post("/reg", qs.stringify({email: refEmail.current.value, password: refPassword.current.value,
            first_name:refFirstName.current.value, last_name: refLastName.current.value})).then(res => {
            window.location.href = "/"
        })
    }

    return (
        <form className={"w-full sm:w-1/3 formContainer"}>
            <div className="mb-6 loginField">
                <label className="labelLoginField block mb-2 text-sm font-medium text-gray-900 dark:text-white">
                    Фамилия</label>
                <input ref={refFirstName} id="email"
                       className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 dark:shadow-sm-light"
                       placeholder="name@flowbite.com" required/>
            </div>
            <div className="mb-6 loginField">
                <label className="labelLoginField block mb-2 text-sm font-medium text-gray-900 dark:text-white">
                    Имя</label>
                <input ref={refLastName} id="email"
                       className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 dark:shadow-sm-light"
                       placeholder="name@flowbite.com" required/>
            </div>
            <div className="mb-6 loginField">
                <label htmlFor="email" className="labelLoginField block mb-2 text-sm font-medium text-gray-900 dark:text-white">
                    Почта</label>
                <input type="email" id="email" ref={refEmail}
                       className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 dark:shadow-sm-light"
                       placeholder="name@flowbite.com" required/>
            </div>
            <div className="mb-6 loginField">
                <label htmlFor="password" className="labelLoginField block mb-2 text-sm font-medium text-gray-900 dark:text-white">
                    Пароль</label>
                <input type="password" id="password" ref={refPassword}
                       className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 dark:shadow-sm-light"
                       required={true}/>
            </div>
            <div className="mb-6 loginField">
                <label htmlFor="repeat-password"
                       className="labelLoginField block mb-2 text-sm font-medium text-gray-900 dark:text-white">Повторите пароль</label>
                <input type="password" id="repeat-password"
                       className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 dark:shadow-sm-light"
                       required/>
            </div>
            <Button style={{margin:10}} variant="contained" color="primary" onClick={handleClick}>
                РЕГИСТРАЦИЯ
            </Button>
        </form>
    );
}
