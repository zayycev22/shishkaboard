import { useCallback, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import {Button, Dialog, DialogActions, DialogContent, TextField} from "@mui/material";
import React, {useState} from 'react';
import axios from "axios";

export const Index = () => {

    const navigateTo = useNavigate()
    const [openDialog, setOpenDialog] = useState(false)
    const [newTitle, setNewTitle] = useState("")
    const [newDescription, setNewDescription] = useState("")
    const [isAuth, setIsAuth] = useState(false)
    const [projects, setProjects] = useState([])

    useEffect(() => {
        axios.get("/check_auth").then(res => {
            setIsAuth(res.data.authenticated)
            if(res.data.authenticated === false) navigateTo("/auth")
            axios.get("/projects").then(res => {
                setProjects(res.data)
            })
        }).catch(err => {
            navigateTo("/auth")
        })
    }, [])

    function createProject(){
        setOpenDialog(true)
    }

    function addProject(){
        let data = {name: newTitle, description: newDescription}
        axios.post("/create_project", data).then(res => {
            axios.get("/projects").then(res => {
                setProjects(res.data)
                setOpenDialog(false)
            })
        })
        setOpenDialog(false)
    }

    return (
        <div className={"baseFlex"}>
            <div className={"baseContainer"}>
                <div className={"titleBaseFlex"}>
                    <div className={"titleWhite"}>Мои проекты</div>
                </div>
                {isAuth &&
                projects.map(e =>
                    <a href={`/project/${e.id}`}>
                        <div className={"projectContainer"}>
                            <div className={"titleProject"}>{e.title}</div>
                            <div style={{display:"flex", flexDirection:"column", marginRight: "4%"}}>
                                <div className={"titleProjectDescription"}>Описание проекта</div>
                                <div className={"projectDescription"}>{e.description}</div>
                            </div>
                        </div>
                    </a>
                )
            }
            <Button onClick={createProject} style={{marginTop:10, marginBottom:10}}>
                Добавить новый проект
            </Button>
            </div>
            <Dialog
                aria-labelledby="DialogTitle"
                aria-describedby="DialogDescription"
                open={openDialog}
                onClose={() => setOpenDialog(false)}
                fullWidth
                maxWidth="md"
            >
                <DialogContent style={{ height: 600 }}>
                    <div className={"baseFlex"}>
                        <TextField
                            value={newTitle}
                            variant="outlined"
                            onChange={e=> setNewTitle(e.target.value)}
                            helperText="Название проекта"
                            style={{width: "90%", margin: 10}}
                        />
                        <TextField
                            value={newDescription}
                            variant="outlined"
                            onChange={e=> setNewDescription(e.target.value)}
                            helperText="Описание проекта"
                            style={{width: "90%", margin: 10}}
                        />
                        <Button onClick={addProject}>Создать проект</Button>
                    </div>
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => setOpenDialog(false)}>Закрыть</Button>
                </DialogActions>
            </Dialog>
        </div>
    )
}

const test = [
    {
        id: 1,
        title: "Разрабока виртуального ассистента",
        description: "Необходимо написать нейросеть, помогающую решить задачи"
    },
    {
        id: 2,
        title: "Предсказание инфляции",
        description: "Необходимо написать нейросеть, для предсказания"
    },
]
