import './App.css'
import React from "react";
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';
import {Index} from "./components/Index.jsx";
import Auth from "./pages/Auth/Auth.jsx";
import Profile from "./pages/Profile/Profile.jsx";
import Project from "./pages/Project/Project.jsx";

function App() {

    return (<Router>
                <Routes>
                    <Route exact path="/" element={<Index/>}/>
                    <Route exact path="/auth" element={<Auth/>}/>
                    <Route exact path="/profile" element={<Profile/>}/>
                    <Route exact path="/project/:id" element={<Project/>}/>
                </Routes>
            </Router>
    );
}

export default App
