import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App'
import './index.css'
import {MyHeader} from './Header.jsx'
import {MyFooter} from "./Footer.jsx";
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';
import axios from "axios";

axios.defaults.baseURL = "http://127.0.0.1:8000/api"
axios.defaults.withCredentials = true;
axios.defaults.headers.common["Content-Type"] = "application/json";

ReactDOM.createRoot(document.getElementById('hd')).render(
    <React.StrictMode>
        <Router>
            <Routes>
                <Route exact path="/" element={<MyHeader/>} />
                <Route exact path="/profile" element={<MyHeader/>} />
                <Route exact path="/project/:id" element={<MyHeader/>} />
                <Route exact path="/auth" element={<MyHeader/>} />
            </Routes>
        </Router>
    </React.StrictMode>,
);

ReactDOM.createRoot(document.getElementById('root')).render(
    <React.StrictMode>
        <App/>
    </React.StrictMode>,
);

ReactDOM.createRoot(document.getElementById('ft')).render(
    <React.StrictMode>
        <MyFooter/>
    </React.StrictMode>,
);
